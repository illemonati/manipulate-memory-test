#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>

void random_function() {
    printf("Function Called! - %p\n", &random_function);
}

void secret_function() {
    printf("Wow, you called the secret function!\n");
}

int main() {
    int number = 233;
    while (1) {
        printf("%d - %p - %ld\n", number, (void *)&number, (long)getpid());
        random_function();
        sleep(2);
    }
}
