extern crate winapi;
extern crate process_memory;
#[macro_use] extern crate detour;

use winapi::shared::minwindef;

#[allow(non_snake_case)] #[no_mangle]
pub extern "stdcall" fn DllMain(_self_handle: minwindef::HINSTANCE, reason: minwindef::DWORD, _lpv_reserved: minwindef::LPVOID) -> minwindef::BOOL {
//    use std::fs::File;
    use winapi::um::winnt;
//    use process_memory::remote_member::LocalManager;

    match reason {
        winnt::DLL_PROCESS_ATTACH => {
//            main_loop();
//            call_secret_function();
            replace_random_function();
            change_number(231);
        },
        winnt::DLL_PROCESS_DETACH => {
        },
        winnt::DLL_THREAD_ATTACH => {
//            main_loop();
//            call_secret_function();
            replace_random_function();
            change_number(231);
        },
        winnt::DLL_THREAD_DETACH => {
        },
        _ => {
        }
    }

    0
}

fn main_loop() {
    loop {
        println!("Injected!");
    }
}

fn change_number(new_number: i32) {
    let address = 0x000000000060FE2Cusize;
    let mut number = address as *mut i32;
    unsafe {
        println!("Number: {}", *number);
        *number = 26i32;
    }




}

fn replace_random_function() {
    type random_function_fn = fn();
    struct FunctionPtrAddress {
        addy: random_function_fn
    }
    let fn_ptrs: FunctionPtrAddress = FunctionPtrAddress {
        addy: unsafe {
            std::mem::transmute::<usize, random_function_fn>(0x0000000000401560)
        }
    };


    static_detours! {
        struct DetourAdd: fn();
    }
    let mut hook = unsafe { DetourAdd.initialize(fn_ptrs.addy, || {println!("hacked function called!");}).unwrap() };
    unsafe { hook.enable().unwrap(); }
}




//still figuring this out
//fn call_secret_function() {
//    extern "C" fn secret_function() {}
//    unsafe {
//        secret_function();
//    }
//}