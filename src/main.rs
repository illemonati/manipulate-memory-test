extern crate process_memory;

use process_memory::TryIntoProcessHandle;
use process_memory::PutAddress;
use process_memory::platform::{self, ProcessHandle};
use std::error::Error;
use std::usize;
use byteorder::{LittleEndian, WriteBytesExt};
use process_memory::Inject;
use std::path::PathBuf;

const ADDRESS: &'static str = "000000000060FE3C";
const NEW_NUM: i32 = 655;

fn main() {
    let mut process_handle = match platform::get_pid("spttw.exe").try_into_process_handle() {
        Ok(handle) => handle,
        Err(err) => {
            println!("Error!\n\n{}", err.description());
            return;
        }
    };
    let address = usize::from_str_radix(ADDRESS, 16).expect("parse error");
//    println!("{}", address);
//    get_number(address, &mut process_handle)
//    set_number(address, &mut process_handle, NEW_NUM);
    inject(&mut process_handle);
}

fn get_number(address: usize, process_handle: &mut ProcessHandle) {
    let number_raw = process_memory::copy_address(address, 1, process_handle).expect("get number error");
    let number = number_raw.get(0).expect("process number error");
    println!("{}", number)
}

fn set_number(address: usize, process_handle: &mut ProcessHandle, new_number: i32) {
    let mut new_num_u8 = vec![];
    new_num_u8.write_i32::<LittleEndian>(new_number).unwrap();
    process_handle.put_address(address, &new_num_u8);
}

fn inject(process_handle: &mut ProcessHandle){
    process_handle.inject(PathBuf::from("dll-sample\\target\\release\\dll_sample.dll").canonicalize().unwrap()).unwrap();
}